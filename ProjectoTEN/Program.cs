﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectoTEN.Data;
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<ProjectoTENContext>(
    // for SQL Server
    // options => options.UseSqlServer(builder.Configuration.GetConnectionString("ProjectoTENContext")));
    
    // for MySQL
    // options => options.UseMySql("server=localhost;initial catalog=projectoten;uid=root;pwd=rasosql", 
    options => options.UseMySql("server=raoliveira.pt;initial catalog=projectoten;uid=root;pwd=rasosql", 
        ServerVersion.Parse("8.0.25-mysql")));

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddSession();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment()) {
    app.UseHttpsRedirection();
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseSession();
// app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.UseForwardedHeaders(new ForwardedHeadersOptions {
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.Run();
